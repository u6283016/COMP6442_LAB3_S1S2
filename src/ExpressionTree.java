public class ExpressionTree {


//    Lit, Mult, Add, Sub, Div, and Minus

    public static void main(String[] args) {
        Expression exp1 = new Lit(5);
        System.out.println(exp1.show() + " evaluates to " + exp1.evaluate());
        Expression exp2 = new Add(new Lit(1), new Lit(1));
        System.out.println(exp2.show() + " evaluates to " + exp2.evaluate());
        Expression exp3 = new Mult(new Lit(3), new Sub(new Lit(3), new Lit(-2)));
        System.out.println(exp3.show() + " evaluates to " + exp3.evaluate());
        Expression exp4 = new Div(new Lit(7), new Minus(new Lit(-3)));
        System.out.println(exp4.show() + " evaluates to " + exp4.evaluate());
    }

    static class Sub extends Expression {
        Expression e1, e2;

        public Sub(Expression e1, Expression e2){
            this.e1 = e1;
            this.e2 = e2;
        }

        @Override
        public String show() {
            return "(" + e1.show() + " - " + e2.show() + ")";
        }

        @Override
        public int evaluate() {
            return e1.evaluate() - e2.evaluate();
        }
    }

    static class Div extends Expression {
        Expression e1, e2;

        public Div(Expression e1, Expression e2){
            this.e1 = e1;
            this.e2 = e2;
        }

        @Override
        public String show() {
            return "(" + e1.show() + " / " + e2.show() + ")";
        }

        @Override
        public int evaluate() {
            return e1.evaluate() / e2.evaluate();
        }
    }

    static class Add extends Expression{
        Expression e1, e2;

        public Add(Expression e1, Expression e2){
            this.e1 = e1;
            this.e2 = e2;
        }

        @Override
        public String show() {
            return "(" + e1.show() + " + " + e2.show() + ")";
        }

        @Override
        public int evaluate() {
            return e1.evaluate() + e2.evaluate();
        }
    }

    static class Mult extends Expression {
        Expression e1, e2;

        public Mult(Expression e1, Expression e2){
            this.e1 = e1;
            this.e2 = e2;
        }

        @Override
        public String show() {
            return "(" + e1.show() + " * " + e2.show() + ")";
        }

        @Override
        public int evaluate() {
            return e1.evaluate() * e2.evaluate();
        }
    }

    static class Lit extends Expression {
        int data;

        public Lit(int data){
            this.data = data;
        }

        @Override
        public String show() {
            if(this.data < 0) return "(" + Integer.toString(this.data) + ")";
            else return Integer.toString(this.data);
        }

        @Override
        public int evaluate() {
            return this.data;
        }
    }

    static class Minus extends Expression {
        int data;
        Expression e;

        public Minus(int data){
            this.data = data;
            this.e = null;
        }

        public Minus(Expression e){
            this.e = e;
            this.data = e.evaluate();
        }

        @Override
        public String show() {
            if (this.e != null) return "(- " + this.e.show() + ")";
            else return "(-" + Integer.toString(this.data) + ")";
        }

        @Override
        public int evaluate() {

            return this.data * -1;
        }
    }

}
